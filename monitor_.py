import bpy
import math

# ==================================================================
# Global variables for the dimension of the screen
# ------------------------------------------------------------------
column=320  # The conceptional screen width
row=180     # The conceptional screen height
scale=20    # The factor by witch the conceptual and integer values 
            # will be divided to obtain the real values
# ==================================================================

# ==================================================================
# Definition of the scene elements of camera and light
# Variable: If "light"=='N' we presume we have to install a adequat
#           
# ------------------------------------------------------------------ 
def scene_set_up(light='N'):
    # Colocation of 3D Cursor
    bpy.context.scene.cursor_location = (-0.5*column/scale,-0.5*row/scale, -0.01)
    # Define variable for actual Scene
    scene = bpy.data.scenes["Scene"]
    # If there is no camera, insert a new one
    if not(bpy.context.scene.camera):
        #print("New camera")
        camera = bpy.data.cameras.new("Camera")
        cam_ob = bpy.data.objects.new("Camera", camera)
        bpy.context.scene.objects.link(cam_ob) 
        bpy.context.scene.camera = cam_ob
    # Colocate the camera to capture only the screen of the monitor
    cam=scene.camera
    z_pos=-0.01-(column/scale)
    cam.location=(0.0,0.0,1.1*z_pos)
    cam.rotation_euler=(3.1415,0.0,3.1415)
    # If the parameter Light is set on 'N' insert a lights for illumination the screen
    if light=='N':
      # We want illuminate the screen without any white reflex on it
      # Doing it with two Sun-lights passing at the extremes of screen
      bpy.ops.object.lamp_add(type='SUN',location=(0,0.0,5*z_pos))
      bpy.context.scene.objects.active.rotation_euler[1]=-5*3.1415/9 
      bpy.ops.object.lamp_add(type='SUN',location=(0,0.0,5*z_pos))
      bpy.context.scene.objects.active.rotation_euler[1]=5*3.1415/9  
# ==================================================================

# ==================================================================
# Define the pixel structure of the screen
# ------------------------------------------------------------------
def screen_topology():
    # Create the topologie of the pixels on screen
    # The first (row+1)*(column+1) vertices are in first list element 
    # of pixels.
    # The first row*column faces are in second list element of pixels
    pixels=[[],[]]                   # pixels=[vertices,faces]
    # Create the pixels
    for j in range (0,row+1):
       for i in range (0,column+1):
          # Create the vertexes for the carrees of the pixel raster
          pixels[0].append([i/scale, j/scale,0.0])
          # Create the carrees that are actualy the pixels
          if (i<column) and (j<row):
              pixels[1].append([(j+1)*(column+1)+i,j*(column+1)+i,j*(column+1)+i+1,(j+1)*(column+1)+i+1])
    return(pixels)
# ==================================================================

# ==================================================================
# Append the surfaces to complete the screen to a monitor box
# ------------------------------------------------------------------
def screen_box_topology():
    # Extension of the screen to a monitor box 
    box= screen_topology()
    # Create 4 outer points of the screen for extending the frontier of screen to a box    
    box[0].append([column/(5*scale),row/(5*scale),1])
    box[0].append([4*column/(5*scale),row/(5*scale),1])    
    box[0].append([column/(5*scale),4*row/(5*scale),1])
    box[0].append([4*column/(5*scale),4*row/(5*scale),1])
    # Create the restant 5 faces of the monitor box
    # Regarding that: The first (row+1)*(column+1) vertices are in first list element of pixels
    #                 The first row*column faces are in second list element of pixels    
    offset=(row+1)*(column+1)
    box[1].append([offset,offset+2,offset+3,offset+1])
    box[1].append([row*(column+1),0,offset,offset+2])
    box[1].append([0,offset, offset+1, column-1])
    box[1].append([column-1, offset-1, offset+3,offset+1])
    box[1].append([offset-1,offset+3,offset+2,(row-1)*(column+1)])
    
    return(box)
# ==================================================================

# ==================================================================
# Extend to the monitorbox the support pilar
# ------------------------------------------------------------------
def monitor_top_topology():
    # Extension of the topology of the monitor box by a supporting column
    top=screen_box_topology()
    # Create the eight vertexes for the supporting column of monitor
    for k in range (0,2):
      for i in range (0,2):
        for j in range (0,2):
          top[0].append([(2+i)*column/(5*scale),(3-4*j)*row/(5*scale),0.1+k*0.5])

    # creating the 6 faces of the supporting column
    # Regarding that: The first row*column+4 vertices are created for the screen box
    #                 The first (row-1)*(column-1)+5 faces are createt for the screen box 
    offset=(row+1)*(column+1)+4
    top[1].append([offset,offset+1,offset+3,offset+2])   # cima
    top[1].append([offset+4,offset+5,offset+7,offset+6]) # baixo
    top[1].append([offset,offset+1,offset+5,offset+4])   # lateral direita
    top[1].append([offset+2,offset+6,offset+7,offset+3]) # lateral esquerda
    
    return(top)
# ==================================================================    
    
# ==================================================================
# Extend to at the pilars of monitor box two feets
# ------------------------------------------------------------------    
def full_monitor_topology():
    # Extending the top of the monitor by a feed base to a full monitor
    monitor= monitor_top_topology() #make_screen_box()

    # Create the eight vertexes for the supporting column of monitor
    for k in range (0,2):
      for i in range (0,2):
          monitor[0].append([(1+3*i)*column/(5*scale),-3*row/(5*scale),4-k*8])
    offset=(row+1)*(column+1)+4
    monitor[1].append([offset+7,offset+3,offset+11])  # triangulo direita baixo
    monitor[1].append([offset+7,offset+3,offset+9])   # triangulo direita cima
    monitor[1].append([offset+1,offset+5,offset+10])  # triangulo esquerdo baixo
    monitor[1].append([offset+1,offset+5,offset+8])  # triangulo equerdo cima
    monitor[1].append([offset+3,offset+1,offset+10,offset+11])  # rectangulo inferior baixo
    monitor[1].append([offset+7,offset+5,offset+10,offset+11])  # rectangulo inferior cima
    monitor[1].append([offset+3,offset+1,offset+8,offset+9])  # rectangulo inferior baixo
    monitor[1].append([offset+7,offset+5,offset+8,offset+9])  # rectangulo inferior cima

    return monitor
# ==================================================================
    
# ==================================================================
# Realize the mesh from both list obtained and giving the material
# ------------------------------------------------------------------
def create_monitor():
    # Get the topology of the monitor
    topology=full_monitor_topology() #monitor_top_topology()

    # Create black, grey and white material
    black = bpy.data.materials.new('Black')
    black.diffuse_color = (0,0,0)
    grey = bpy.data.materials.new('Grey')
    grey.diffuse_color = (0.5,0.5,0.5)
    white = bpy.data.materials.new('White')
    white.diffuse_color = (1,1,1)

    # Create mesh object
    bpy.ops.object.add(type='MESH')
    obj=bpy.context.object
    obj.name="Screen"
    mesh=obj.data
    mesh.materials.append(black)
    mesh.materials.append(grey)
    mesh.materials.append(white)
        
    # fill data in the mesh with topology
    mesh.from_pydata(topology[0],[],topology[1]) 

    # Set all pixel on black color
    for i in range (0, row*column):
         mesh.polygons[i].material_index = 0
    # Set all other surfaces on grey color     
    for i in range (row*column,row*column+17):
         mesh.polygons[i].material_index = 1

    # update the mesh    
    mesh.update()   
    return(obj)
# ==================================================================

# ==================================================================
# Paint the sceen on a selected position with a selected color
# ------------------------------------------------------------------
def set_pixel(x,y,screen,color_index=2):
      screen.data.polygons[(row-y)*column-x-1].material_index = color_index  # Alteracoes no codigo para que o Ponto(0,0) seja o canto superior esquerdo
# ==================================================================

# ==================================================================
# Clear the monitor for beeing black on every pixel
# ------------------------------------------------------------------
def clear_monitor(screen):
    for i in range(0,row):
        for j in range (0,column):
          set_pixel(j,i,screen,0)
# ==================================================================

# ==================================================================
# Draw a line
# ------------------------------------------------------------------
def line(p,q,monitor):
    m=0
    # The parameter switch is to guarantee that we passing the bigger of
    # the intervals [x0,x1] or [y0,y1] as argument for creating the line
    switch=0
    if not (p[1]==q[1]): m=(q[0]-p[0])/(q[1]-p[1])
    if m>1 or m<-1: switch=1
    # If switch==0 we use the normal canfiguration with x as argument
    # and y as result of the function.
    # Else we switch the role
    x0=(1-switch)*p[0]+switch*p[1]
    x1=(1-switch)*q[0]+switch*q[1]
    y0=(1-switch)*p[1]+switch*p[0]
    y1=(1-switch)*q[1]+switch*q[0]
    # If switch==0 we use the normal canfiguration with x as argument
    # and y as result of the function.
    # Else we switch again the role for beeing written in the right order
    mirrow=1
    if (x1<x0): mirrow=-1
    for i in range (0,mirrow*(x1-x0+1)):
        y=round(y0+m*mirrow*i,0)
        set_pixel(int((1-switch)*(x0+i)+switch*y),int((1-switch)*y+switch*(x0+i)),monitor,2)        
# ==================================================================

def brensenham_line(x_a, y_a, x_b, y_b, screen, color_index):
    # Condições iniciais
    x_a, y_a = x_a, y_a
    x_b, y_b = x_b, y_b
    diferencial_x = x_b - x_a
    diferencial_y = y_b - y_a
 
    # Determina a inclinação da linha
    inclinacao = abs(diferencial_y) > abs(diferencial_x)
 
    # Efectua a rotação da linha
    if inclinacao:
        x_a, y_a = y_a, x_a
        x_b, y_b = y_b, x_b
 
    # Troca o ponto de inicio e de fim caso necessario e armazena o estado da troca
    trocado = False
    if x_a > x_b:
        x_a, x_b = x_b, x_a
        y_a, y_b = y_b, y_a
        trocado = True
 
    # Recalcular diferenciais
    diferencial_x = x_b - x_a
    diferencial_y = y_b - y_a
 
    # Calcular erro
    erro = int(diferencial_x / 2.0)
    inclinacao_y = 1 if y_a < y_b else -1
 
    # Percorre as caixas delimitadoras gerando pontos entre o inicio e o fim
    y = y_a
    #points = []
    for x in range(x_a, x_b + 1):
        coord = (y, x) if inclinacao else (x, y)
        set_pixel(coord[0], coord[1], screen, color_index)
    
        erro -= abs(diferencial_y)
        if erro < 0:
            y += inclinacao_y
            erro += diferencial_x

# Preenchimento do poligono
def preenchimento_poligono(poligono, monitor, color_index):
   
    # Tabela de lados [ymax, x(ymin), 1/m, referencia ao proximo lado]
    tabela_lados = {}  # Y : lado
    
    # Lados
    for i in range(-1, len(poligono) - 1):
        
        lado = []
        y_max = max(poligono[i][1], poligono[i+1][1])
        y_min = min(poligono[i][1], poligono[i+1][1])
        lado.append(y_max)
        
        # x do y min   
        x = min(poligono[i], poligono[i+1], key=lambda x:x[1])[0]
        lado.append(x)
        
        # 1/m
        delta_y = poligono[i+1][1] - poligono[i][1]
        delta_x = poligono[i+1][0] - poligono[i][0]
        
        # Adicionar se nao for um reta horizontal
        # TODO: Passar isto para o principio para ser mais eficiente
        if delta_y != 0:
            declive = delta_x / delta_y
        else: # se for um reta horizontal passar a frente
            continue 
        
        lado.append(declive)
        
        # Adicionar a referencia ao proximo lado  
        # se nao houver um lado com o y min 
        if y_min not in tabela_lados:
            tabela_lados[y_min] = [lado]
        
        else: # Verificar se o lado que esta la dentro tem o mesmo valor do ymin do lado corrente 
            
            tabela_lados[y_min].append(lado) 
         
    # Ordenar pelo valor de y_max 
    # Nao sei se e necessario ... 
    # Fiz para ficar igual a tabela exemplo do livro, pag21
    for key in tabela_lados:
        lados = tabela_lados[key]
        lados.sort(key = lambda x: x[0])
     
        
    # Construcao da tabela de lados ativos
    tabela_lados_ativos = []
    
    # Ordenada minima do poligono
    y = min(poligono, key = lambda x:x[1])[1]
    maxi = max(poligono, key = lambda x:x[1])[0]
  
    while True: 
        if (len(tabela_lados) == 0 and len(tabela_lados_ativos) == 0):
            return
        else: 
            # 5 mover da tabela de lados para a tabela de lados ativos 
            if y in tabela_lados:
                for l in tabela_lados[y]:
                    tabela_lados_ativos.append(l)
                tabela_lados.pop(y) 
                        
            lados = tabela_lados_ativos
            lados.sort(key = lambda x: x[1])
            tabela_lados_ativos = lados
           
            # 6 Preencher os spans
            for i in range(0, len(tabela_lados_ativos)-1, 2):            
                x1 = int(tabela_lados_ativos[i][1])
                x2 = int(tabela_lados_ativos[i+1][1])
                brensenham_line(x1,y,x2,y,monitor, color_index)
                
            # 7 Remover da tabela lados_ativos
            for l in tabela_lados_ativos[:]:
                if y == l[0] - 1:
                    tabela_lados_ativos.remove(l)                  
            
            # 8 
            y += 1
            
            # 9
            for i in range(len(tabela_lados_ativos)):
                tabela_lados_ativos[i][1] += tabela_lados_ativos[i][2]
                           
# Faz o outline do poligono            
def outline_poligon(poligono, monitor, color_index): 
    for i in range(-1, len(poligono)-1):
        brensenham_line(poligono[i][0], poligono[i][1], poligono[i+1][0], poligono[i+1][1], monitor, color_index)       
# ==================================================================

# Elimina todos os objecto que contenham mesh na cena
def delete_all():
    # select objects by type
    for o in bpy.data.objects:
        o.select = True
        
    # call the operator once
    bpy.ops.object.delete()

# Painting a line on the monitor
# ------------------------------------------------------------------
def __main__(): #Função main
    delete_all()
    scene_set_up()
    monitor=create_monitor()
    ##x1 = 0
    ##y1 = 0
    ##x2 = 30
    ##y2 = 100
    
    # Teste Poligono
    poligono = [[20, 30],[70, 10],[65, 50], [130, 50],[50, 110],[50, 50], [20,90]] #Define um poligno com 7 pontos
    poligono_com_outline(poligono, monitor) #Desenha o poligno com outline
        
	# Chama a função responsavel pelo calculo dos pontos de simetria
	# e pelo desenho da circunferencia 
    circunferencia(100,75,20,monitor,color_index=1)
    
    bpy.ops.render.render(write_still=True)
# ==================================================================

def circunferencia(Ox, Oy, R, monitor, color_index):
	d = int(1 - R)
	deltaE = int(3) #Inicializa deltaE a 3
	deltaSE = int(-2 * R + 5)
	x = int(0) #Inicializa x a zero
	y = int(R) #Inicializa y igual ao tamanho do raio	
	circunfrencia_auxiliar(x,y,R,Ox,Oy,monitor,color_index) # Desenha o eixo das abcissas e ordenadas com o ponto de intersecção no centro da circunferencia
	while y > x: # Percorre todos os pixeis e encontra o ponto medio que se encontra entre deltaSE e deltaE,
				 # selecionando aquele que tem menor distancia entre um dos 
		if d < 0: #Verifica se o ponto encontra-se no interior da circunferencia
			d = d + deltaE
			deltaE = deltaE + 2 #Incrementa deltaE em 2
			deltaSE = deltaSE + 2 #Incrementa deltaSE em 2
		else: #Verifica se o ponto se encontra no exterior da circunferencia
			d = d + deltaSE
			deltaE = deltaE + 2 #Incrementa deltaE em 2
			deltaSE = deltaSE + 4 #Incrementa deltaSE em 4
			y = y - 1 #Decrementa y em 1
		x = x + 1 #Incrementa x em 1
		circunfrencia_auxiliar(x,y,R,Ox,Oy,monitor,color_index) # Desenha a circunferencia

def circunfrencia_auxiliar(x,y,R,Ox,Oy,monitor,color_index):
	PONTOS = [[x+Ox,y+Oy],[y+Ox,x+Oy],[y+Ox,-x+Oy],[x+Ox,-y+Oy],[-x+Ox,-y+Oy],[-y+Ox,-x+Oy],[-y+Ox,x+Oy],[-x+Ox,y+Oy]] # Lista que contem as coordenadas necessarias
																													   # para o calculo dos pontos A a H assim como 
																													   # os pontos intermedios de ligação entre cada
																													   # um deles que por sua vez definem a circunferencia
	# SSSSSSSSSSSSSSSSSSSSSSSSSSS
	# S    HJJJA                S
	# S  GKKKKKKKB              S
	# S  FLLLLLLLC              S
	# S    EMMMD                S
	# S                         S
	# S                         S
	# SSSSSSSSSSSSSSSSSSSSSSSSSSS
	#          SS     SS
	#          SS     SS
	#          SS     SS
	#         SS       SS
	#        SS         SS
	#       SS           SS
	#      SSSSSSSSSSSSSSSSS
	
	brensenham_line(-x+Ox+R,-y+1+Oy+R,x+Ox+R,-y+1+Oy+R,monitor,color_index=1) #Faz o preenchimento da area "J" na imagem acima
	brensenham_line(y+Ox+R,-x+Oy+R,-y+Ox+R,-x+Oy+R,monitor,color_index=1)     #Faz o preenchimento da area "K" na imagem acima
	brensenham_line(y+Ox+R,x+Oy+R,-y+Ox+R,x+Oy+R,monitor,color_index=1)       #Faz o preenchimento da area "L" na imagem acima
	brensenham_line(x+Ox+R,y-1+Oy+R,-x+Ox+R,y-1+Oy+R,monitor,color_index=1)   #Faz o preenchimento da area "M" na imagem acima
	
	for ponto in PONTOS: #Percorre os pontos de 'A' a 'H' 
		set_pixel(ponto[0]+R, ponto[1]+R, monitor, color_index=2) #Desenha um dos pontos da circunferencia, por iteração,
																  #no ecra, no fim do ciclo define a toda a circunferencia
# Desenhar poligono com outline
def poligono_com_outline(poligono, monitor):
    preenchimento_poligono(poligono, monitor, color_index=1)
    outline_poligon(poligono, monitor, color_index=2)
    
__main__()